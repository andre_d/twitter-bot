# twitter-bot

Welcome. To use this bot, you must first install all required packages:

```pip3 install -r requirements.txt```

Enter the information in: ```env.sh```
You must enter:
- your Twitter API access codes
- your Elasticsearch access codes


Once installed, you have two options:
- Run the main thread, with: ```python3 main.py```
<img src="https://gitlab.com/andre_d/twitter-bot/-/raw/master/doc/setup.png"  height="120">

- Run the updater thread, with: ```python3 update.py```
<img src="https://gitlab.com/andre_d/twitter-bot/-/raw/master/doc/update.png"  height="120">

