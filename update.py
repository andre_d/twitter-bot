from elasticsearch import Elasticsearch
es = Elasticsearch('http://elastic:changeme@192.168.2.11:9200')
import time
import requests
from os import getenv

bearer_token = getenv('BEARER_TOKEN')

def get_tweet(id: str):
    res = requests.get(f'https://api.twitter.com/2/tweets/{id}?tweet.fields=public_metrics', headers={'Authorization': f'Bearer {bearer_token}'})
    if res.status_code == 200:
        body = res.json()
        if 'data' in body and 'public_metrics' in body['data']:
            return body['data']['public_metrics']
    return None

def update(raw: dict, metrics: dict):
    raw['_source']['likes'] = metrics['like_count']
    raw['_source']['replies'] = metrics['reply_count']
    raw['_source']['retweets'] = metrics['retweet_count']
    raw['_source']['quotes'] = metrics['quote_count']

    return es.update(index='twitter', doc_type='_doc', id=raw['_id'], body={'doc': raw['_source']})

query = { 'range': {
          'timestamp': {
            'gte': 'now-1d/d', 
            'lt': 'now/d'
            }
        } 
    }

res = es.search(index='twitter', body={'size': 0, 'query': query})
count = res['hits']['total']['value']

for i in range(1, int(count/100)):
    res = es.search(index='twitter', body={'size': 100, 'from': i, 'query': query})
    for tweet in res['hits']['hits']:
        if '_source' in tweet and 'id' in tweet['_source']:
            time.sleep(0.01)
            metrics = get_tweet(tweet['_source']['id'])
            if metrics:
                try:
                    update_res = update(tweet, metrics)
                    print(update_res)
                except Exception:
                    print('Failed to update: {e}')
    time.sleep(1)
