import twitter
from os import getenv
import logging
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from datetime import datetime
import json


# Get twitter API credentials
consumer_key=getenv('CONSUMER_KEY')
consumer_secret=getenv('CONSUMER_SECRET')
access_token_key=getenv('ACCESS_TOKEN_KEY')
access_token_secret=getenv('ACCESS_TOKEN_SECRET')

# Get elasticsearch information
elastic_host=getenv('ELASTIC_HOST', 'localhost')
elastic_port=getenv('ELASTIC_PORT', 9200)
elastic_user=getenv('ELASTIC_USER', 'elastic')
elastic_password=getenv('ELASTIC_PASSWORD', 'elastic')

# Create elasticsearch api client
es = Elasticsearch(f'http://{elastic_user}:{elastic_password}@{elastic_host}:{elastic_port}')
es.indices.create(index='twitter', ignore=400)

# Create twitter api client
api = twitter.Api(consumer_key=consumer_key,
                  consumer_secret=consumer_secret,
                  access_token_key=access_token_key,
                  access_token_secret=access_token_secret
                  )


terms = ['cybersecurity', 'cybersécurité', 'cybersecurite', 'cyberattaque', 'cyberattack', 'cyberdefense', 'cyberdéfense', 'FIC2021', 'FIC_eu']


def parse_tweets(tweet: dict) -> list:
    tweets = []
    tweets.append({})
    parsed = tweets[0]
    parsed['_index'] = 'twitter' # Elastic index
    parsed['id'] = tweet['id']
    parsed['source'] = tweet['source']
    parsed['text'] = tweet['text']
    parsed['creation_date'] = tweet['created_at']
    parsed['timestamp'] = datetime.strptime(parsed['creation_date'], '%a %b %d %H:%M:%S +0000 %Y')
    parsed['quotes'] = tweet['quote_count'] if 'quote_count' in tweet else 0
    parsed['replies'] = tweet['reply_count'] if 'reply_count' in tweet else 0
    parsed['retweets'] = tweet['retweet_count'] if 'retweet_count' in tweet else 0
    parsed['likes'] = tweet['favorite_count']
    parsed['reply'] = tweet['in_reply_to_status_id'] is not None
    parsed['retweet'] = tweet['retweeted_status']['id'] if 'retweeted_status' in tweet else None
    parsed['locale'] = tweet['lang']
    parsed['user'] = {
        'id': tweet['user']['id'],
        'name': tweet['user']['name'],
        'handle': tweet['user']['screen_name'],
        'verified': tweet['user']['verified'],
        'followers': tweet['user']['followers_count'],
        'friends': tweet['user']['friends_count'],
        'creation_date': tweet['user']['created_at'],
        'geo': tweet['user']['geo_enabled']
    }
    parsed['geo'] = tweet['geo']
    parsed['coordinates'] = tweet['coordinates']
    parsed['hashtags'] = [{'text': x['text']} for x in tweet['entities']['hashtags']]
    parsed['mentions'] = [{'id': x['id'], 'name': x['name'], 'handle': x['screen_name']} for x in tweet['entities']['user_mentions']]

    if parsed['retweet'] is not None:
        tweets.append({})
        tweets[1] = parse_tweets(tweet['retweeted_status'])[0]

    return tweets


# Real time

for t in api.GetStreamFilter(track=terms):
    b = parse_tweets(t)
    try:
        bulk(es, b, refresh='wait_for')
    except Exception as e:
        print(f'Failed to bulk: {e}')
