#!/bin/sh

# Twitter
export CONSUMER_KEY=
export CONSUMER_SECRET=
export ACCESS_TOKEN_KEY=
export ACCESS_TOKEN_SECRET=
export BEARER_TOKEN=

# ElasticSearch
export ELASTIC_HOST=
export ELASTIC_PORT=9200
export ELASTIC_USER=elastic
export ELASTIC_PASSWORD=
